# -*- org-confirm-babel-evaluate: nil; -*-
#+title: Softwares and String Figures
#+date: <2024-03-13>
#+language: en
#+bibliography: ../string-figures.bib
#+cite_export: csl ../author-date.csl
#+email:

#+beamer_header: \institute{Sphere}

#+startup: beamer
#+latex_class: beamer

#+beamer_header: \usepackage{caption}
#+beamer_header: \usepackage{subcaption}

#+options: toc:nil

#+beamer_frame_title: 3
#+beamer_frame_level: 3
#+beamer_header: \usetheme{upcite}
#+beamer_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Plan}\tableofcontents[currentsection, currentsubsection]\end{frame}}

#+latex_header: \newcommand{\fname}[1]{\textsc{#1}}

* Introduction

***** The "Owl"                                                 :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* References                                              :BMCOL:
:PROPERTIES:
:BEAMER_col: .4
:END:

- Alfred \fname{Tozzer} and \fname{Haddon} in Chicago [cite:@haddonFewAmericanString1903;pp.219-222]
- Zah \fname{Tso} and his sister in Gallup [cite:@jayneStringFiguresHow1962;pp.48-56]

******* Map                                                     :BMCOL:
:PROPERTIES:
:BEAMER_col: .6
:END:
#+CAPTION: Places where versions of the "Owl" have been collected  -- Map data: Natural Earth & geonames.org.
#+RESULTS: draw_meetings_map

***** String Figures-Making as a "procedural" activity          :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* A "procedural activity"                       :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:
[cite:@vandendriesscheStringFiguresMathematics2015;p.4,27]

******* Programs or routines                                :B_columns:
:PROPERTIES:
:BEAMER_env: columns
:END:

********* [cite:@turingComputingMachineryIntelligence1950;p.438] :B_block:BMCOL:
:PROPERTIES:
:BEAMER_env: block
:BEAMER_col: .5
:END:
#+begin_quote
Constructing instruction tables is usually described as "programming." To "programme a machine to carry out the operation A" means to put the appropriate instruction table into the machine so that it will do A.\\
#+end_quote
********* [cite:@shepherdsonComputabilityRecursiveFunctions1963;220] :B_block:BMCOL:
:PROPERTIES:
:BEAMER_env: block
:BEAMER_col: .5
:END:
#+begin_quote
So we define a /program/ (or routine) as a finite sequence of $l$ /lines/, each line being of the form $I[m_1, \dots, m_k]$, where $I[E1,\dots, Ek]$ is an instruction, $k$ is the number of non-normal exits of $I$, and $m_1,\dots,m_k$ are integers between 1 and $l+1$ (where it is understood that if $k=0$ the line simply consists of $I$ alone).\\
#+end_quote


* Evolving programs

***** A need for updates                                        :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:
******* ANSI's Security alert CERT-FR-2023-ALE-006: https://www.cert.ssi.gouv.fr/alerte/CERTFR-2023-ALE-006/ --- consulted on 12 March 2024 :B_example:
:PROPERTIES:
:BEAMER_env: example
:END:

********* Table                                               :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

[[../img/CERT-FR-2023-ALE-006-tableau.png]]

********* Summary                                             :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

[[../img/CERT-FR-2023-ALE-006-resume.png]]


******* See also

[cite:@notinAuCoeurAction2024;pp.31,34-35].

*** Git versioning
***** Git                                                       :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

#+CAPTION: Linus Torvard, "Initial revision of "git", the information manager from hell". Git, commit e83c516, April 8, 2005 (https://github.com/git/git/commit/e83c5163316f89bfbde7d9ab23ca2e25604af290, consulted on March 8th 2024)
#+ATTR_LATEX: :height \paperheight - 10em
[[../img/Git-definition-TORVALDS.png]]

*** Semantic versioning
***** Semver 2.0.0                                              :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

*******                                                         :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

1. major changes can lead to incompatibilities with previous versions;
2. minor changes add or modifie some functionalities, but could easily replace previous verions;
3. patches are made to fix bugs or minor issues.

*******  [cite:@semver]                                 :B_block:BMCOL:
:PROPERTIES:
:BEAMER_env: block
:BEAMER_col: .5
:END:

#+begin_quote
By giving a name and clear definition to the above ideas, it becomes easy to communicate your intentions to the users of your software. Once these intentions are clear, flexible (but not too flexible) dependency specifications can finally be made.
#+end_quote


***** Two Owls                                                  :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

#+CAPTION: First (left) and second (right) owl by Rui \fname{Yuan}, Paris 2nd March 2024
#+NAME: fig-raw_owls
#+begin_figure
#+begin_center
#+ATTR_LATEX: :width .4\textwidth :center
[[../img/Owl_1-YUAN-2024_03_02.JPG]]
#+ATTR_LATEX: :width .4\textwidth :center
[[../img/Owl_2-YUAN-2024_03_02.JPG]]
#+end_center
#+end_figure
***** Two Owls (2)                                              :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:
#+CAPTION: First (left) and second (right) owl by Rui \fname{Yuan}, Paris 2nd March 2024 with difference in crossings marked in red
#+NAME: fig-crossed_owls
#+begin_figure
#+begin_center
#+ATTR_LATEX: :width .4\textwidth :center
[[../img/Owl_1-YUAN-2024_03_02-crossings.png]]
#+ATTR_LATEX: :width .4\textwidth :center
[[../img/Owl_2-YUAN-2024_03_02-crossings.png]]
#+end_center
#+end_figure

* Two more things
*** Forks and merges
***** The concept of branches                                   :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

#+CAPTION: Extract from an open-source project's history (Maxime \fname{Berger}, Clément \fname{Cartier}, Frédéric \fname{Jaëck} & all. /Morceaux choisis de géométrie/)
#+ATTR_LATEX: :height \paperheight - 10em
[[../img/MCDG-extract-history.png]]

***** [cite:@delalandeChapitreVIJeux2001;§230]                  :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* My translation                                  :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:
#+begin_quote
By playing together, each brings his or her own personal note and tries to make it accepted by others. As in the rope game, transformations come less from an imitative process than from an adaptation to the players' abilities and from their desire to add their small innovations to the game. […] Variants allow a group of friends to make their own a game practiced by all. The addition of an element, of a difficulty, gives a familiar game a new attraction.
#+end_quote
******* Original                                        :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:
#+begin_quote
/En jouant à plusieurs, chacun apporte sa note personnelle et cherche à la faire accepter des autres. Comme au jeu de corde, les transformations viennent moins d'un processus imitatif que d'une adaptation aux capacités des joueurs et de leur désir d'ajouter au jeu leurs petites innovations. […] Les variantes permettent à un groupe d'amis de s'approprier un jeu pratiqué par tous. L'ajout d'un élément, d'une difficulté, donne un attrait nouveau à un jeu familier./
#+end_quote

*** Names

***** It's a feature, not a bug                                 :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* Android                                                 :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Sculptures made for different versions of Android in the GooglePlex at Montain View, California. Image: Runner1928, Wikimedia commons ©CC-BY-SA
[[../img/Android_building_in_Googleplex_with_sculptures.jpg]]

******* Bug                                                     :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:
#+CAPTION: "It's not a BUG Its a FEATURE" (Forester-Joe, deviantart.com, 30 June 2008, made with Paint.Net v3.31: https://www.deviantart.com/forester-joe/art/Its-not-a-BUG-Its-a-FEATURE-90226561 ©CC-BY-NC)
[[../img/Bug_feature-FORESTER_JOE-2008.jpg]]

See [cite:@espinosaAppleII1979;p.180]

***** [cite:@delalandeChapitreVIJeux2001;§18]                   :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* My translation                                  :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:
#+begin_quote
Each child is free to give the game a new appeal by giving a new name to the sand he gets from a particula place. This is why, if part of the technique is relatively stable […], the room for invention is large and can allow a child to get noticed by his peers.
#+end_quote

******* Original quote                                  :B_block:BMCOL:
:PROPERTIES:
:BEAMER_env: block
:BEAMER_col: .5
:END:
#+begin_quote
/Chaque enfant est libre de donner un attrait supplémentaire au jeu en trouvant un nouveau nom pour un sable qu'il ira chercher à un endroit particulier. C'est pourquoi, si une partie des techniques est relativement stable […], la place de l'invention est grande et elle peut permettre à un enfant de se faire remarquer de ses pairs./
#+end_quote

* Conclusion

*** Conclusion                                                    :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

#+CAPTION: Chemlaian hammer --- from a picture by Rui \fname{Yuan} (8 February 2024)
#+ATTR_LATEX: :height \paperheight - 10em
[[../img/Chemlaian-hammer-YUAN.png]]
* Bibliography
*** Bibliography                                                  :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:
#+print_bibliography:
* Annexes :noexport:
*** Source code for drawing maps
#+NAME: draw_map
#+begin_src python :session :exports both
  """ Plotting locations on a map """

  import geocoder
  import matplotlib.pyplot as plt
  import cartopy
  from cartopy.mpl.geoaxes import GeoAxes

  Place = tuple[str, float, float] # Name, longitude, latitude
  Legend = tuple[
      str,                         # Text of the legend
      float,                       # Text's x coordinate
      float,                       # Text's y coordinate
      float,                       # Point's x coordinate
      float,                       # Point's y coordinate
  ]
  Position = tuple[float, float]  # x, y

  MAP_MARGIN_X = 1              # Distance from western point to map's border
  MAP_MARGIN_Y = 1              # Distance from southern point to map's border
  LEGENDS_DEFAULT_OFFSET_X = .5 # Distance from a point to it's legend (x)
  LEGENDS_DEFAULT_OFFSET_Y = .5 # Distance from a point to it's legend (y)
  LEGENDS_OFFSET_MARGIN_Y = .1  # Distance between two legends

  def draw_map(places_names: list[str], filename: str, meridian: float = 159) -> str:
      ax = init_map(meridian)
      places = [get_place_informations(name, meridian) for name in places_names]
      draw_places(places, ax)
      plt.tight_layout()
      plt.savefig(filename)
      return filename

  def init_map(meridian: float = 159) -> GeoAxes:
      ax = plt.axes(projection=cartopy.crs.PlateCarree(central_longitude=meridian))
      ax.coastlines()
      ax.add_feature(cartopy.feature.OCEAN)
      ax.add_feature(cartopy.feature.LAKES)
      ax.add_feature(cartopy.feature.LAND, alpha=.3)
      ax.add_feature(cartopy.feature.RIVERS)
      ax.add_feature(cartopy.feature.STATES, alpha=.1)
      return ax

  def get_place_informations(name: str, meridian: float = 180) -> tuple[str, float, float]:
      geonames_id = geocoder.geonames(name, key=geonames_key).geonames_id
      data = geocoder.geonames(geonames_id, method="details", key=geonames_key)
      longitude = float(data.lng)
      return (data.address, longitude, float(data.lat))

  def draw_places(places: list[Place], ax: GeoAxes):
      legends = []
      for place in places:
          draw_place(place, ax)
          compute_legend_position(place, legends)
      for legend in legends:
          draw_legend(legend, ax)

  def draw_place(place: Place, ax: GeoAxes):
      ax.plot([place[1]], [place[2]], marker="o", color="red", markersize=4, transform=cartopy.crs.PlateCarree())
      # Make sure to have margins
      ax.plot([place[1] + MAP_MARGIN_X], [place[2] + MAP_MARGIN_Y], transform=cartopy.crs.PlateCarree())
      ax.plot([place[1] - MAP_MARGIN_X], [place[2] - MAP_MARGIN_Y], transform=cartopy.crs.PlateCarree())

  def draw_legend(legend: Legend, ax: GeoAxes):
      ax.annotate(
          legend[0],
          xytext=(legend[1], legend[2]),
          xy=(legend[3], legend[4]),
          arrowprops=dict(arrowstyle="->"),
          fontsize=12, wrap=True,
          horizontalalignment="center",
          transform=cartopy.crs.PlateCarree(),
      )

  def compute_legend_position(place: Place, other_legends: list[Legend]):
      x = place[1] + LEGENDS_DEFAULT_OFFSET_X
      y = compute_legend_y((x, place[2]), other_legends)
      other_legends.append((place[0], x, y, place[1], place[2]))

  def compute_legend_y(pos: Position, other_legends: list[Legend]) -> float:
      offset = LEGENDS_DEFAULT_OFFSET_Y
      for other in other_legends:
          offset = avoid_conflict(pos, offset, other)
      return pos[1] + offset

  def avoid_conflict(pos: Position, offset: float, other: Legend) -> float:
      new_pos = (pos[0], pos[1] + offset)
      if are_legends_conflicting(new_pos, other):
          return offset * -1
      return offset

  def are_legends_conflicting(pos: Position, other: Legend) -> bool:
      return LEGENDS_OFFSET_MARGIN_Y > abs(pos[1] - other[2])

  def is_above(pos: tuple[float, float], other: Legend) -> bool:
      return pos[1] >= other[2]

#+end_src

#+header: :noweb strip-export
#+NAME: draw_meetings_map
#+begin_src python :results value file :var matplot_lib_filename=(org-babel-temp-file "local_map" ".png") :var geonames_key="catgolin" :exports both
  <<draw_map>>
  MAP_MARGIN_X = 7
  MAP_MARGIN_Y = 10
  LEGENDS_DEFAULT_OFFSET_X = 0
  LEGENDS_DEFAULT_OFFSET_Y = 3
  LEGENDS_OFFSET_MARGIN_Y = 3

  places = ["New Mexico", "Chicago", "Nanizhoozhi"]
  return draw_map(places, matplot_lib_filename)
  #+end_src


* Dropped directions                                               :noexport:
*** Requirements and dependency
#+begin_quote
Ils [les enfants] mettent en jeu une inventivité et une créativité qui résultent certainement du mode d'apprentissage de la formulette. Celui-ci ne se fait ni par un mot à mot ni par une explication du sens qui souvent reste obscur (comme dans Am stram gram), mais d'un seul bloc et dans l'action d'un jeu. Par conséquent, quand une formulette est prononcée, elle est le résultat d'une « remémoration générative » (Jack Goody, 1977 : 40) de la part de l'enfant qui la fait vivre. Par cette analyse, les variantes des formulettes enfantines prennent une autre couleur. Elles ne sont pas de l'ordre d'une faute, du moins quand elles sont faites dans les règles de l'art, mais elles témoignent d'une capacité des enfants à trouver dans l'instant un mot de remplacement qui réponde aux règles de cet art
[cite:@delalandeChapitreVIJeux2001;§243]
#+end_quote
*** How to describe the making of double-sided lozenges
***** Introduction
For today's presentation, I want to use String Figures as an excuse to discuss the notion of "procedure" as we use it in history of mathematics.
In his PhD dissertation under the supervision of Karine Chemla and Sophie Derosiers, Eric Vandendriescche indeed presented String Figures-making as a kind of "procedural activity"[fn:: "The issue of recognizing an activity as mathematical will be addressed in this volume through the analysis of the procedural activity of "string figure-making"." [cite:@vandendriesscheStringFiguresMathematics2015;p.4]; "The process of making a string figure can be analysed as a series of "simple movements" that I call "elementary operations", insofar as the making of any string figure can be described by referring to a certain number of these operations. A string figure can thus be seen as the result of a "procedure" consisting of a succession of elementary operations" [cite:@vandendriesscheStringFiguresMathematics2015;p.27]], in many ways related to mathematical activities.
#+latex: \displaytime[30]

I will limit myself to the analysis of the making of a few figures that Eric Vandendriessche has categorised as "double-sided lozenge" figures.

The first one was described by Honor Maude in his compilation of Sir Raymond Firth's and Christa de Coppet's notes published in 1978 [cite:@maudeSolomonIslandsString1978d;pp.1-2]. It was shown to Raymond Firth by Eric Nora on the Fenualoa Island, under the name of /Niu/, which means "star". According to him, it is also known in the Pileni Island under the name of /Fitu/ ---also meaning "star"--- and Christa de Coppet knew it under the name of /Uume/ ---designating a shell breast ornament for women--- in the Takataka Island.
#+latex: \displaytime[40]

#+CAPTION: Description of Niu in [cite:@maudeSolomonIslandsString1978d;pp.1-2]
[[file:img/SPHERE-AHPANMM-2024_03-CARTIER-Niu-MAUDE1978.png]]

The second one is called "Egg" by Yukio Shishido and Hiroshi Noguchi [cite:@Shishido-papou;p.51] who "collected" it in the Highlands of Papua New Guinea.
#+latex: \displaytime[15]

To make the last one, we start exactly the same way as for the "Egg". However, we divert to produce a figure that, according to Robert Harold Compton, is called /Watasith/ ---"Star"--- in the Lifou Island [cite:@comptonStringFiguresNew1919;p.217]. This "Star" figure is an intermediate step for other figures, such as the /Misima/ collected by Eric Vandendriessche in the Trobriand Island.
We then proceed to make the final figure, which Yukio Shishido and Hiroshi Noguchi called "Moon" [cite:@Shishido-papou;p.51].
By changing the fingers on which we present it, we can see that it is identical ---in terms of crossings--- to the "Egg".
#+latex: \displaytime[40]

#+CAPTION: The "Star" figure as an intermediate step of the /Misima/ figure making, collected by Eric Vandendriessche in the Trobriand Islands (http://www.rehseis.cnrs.fr/www/vandendriessche/kaninikula/OpeningsA/OpeningA/sg-misima/44.Misima/44.Misima.html)
[[../img/misima-ending-13.jpg]]

#+CAPTION: Location of the Solomon Islands (source: geonames.org)
#+RESULTS: draw_global_map

#+CAPTION: Map of the islands cited (source: geonames.org)
#+RESULTS: draw_local_map

People who were making these figures before we came to "collect" them did not use to write down instructions on how to make them.
However, some part of the litterature dedicated to String Figures have tried to document this practice by writing a finite sequence of instructions describing, step by step, the actions performed in the making of each particular figure.
Such a finite sequence of instructions corresponds to a widly adopted definition for the notion of program, or routine, in computer science.
#+latex: \displaytime[30]

#+CAPTION: Quotes on the definition of program
#+begin_figure
#+begin_quote
Constructing instruction tables is usually described as "programming." To "programme a machine to carry out the operation A" means to put the appropriate instruction table into the machine so that it will do A.
[cite:@turingComputingMachineryIntelligence1950;p.438]
#+end_quote

#+begin_quote
"So we define a /program/ (or routine) as a finite sequence of $l$ /lines/, each line being of the form $I[m_1, \dots, m_k]$, where $I[E1,\dots, Ek]$ is an instruction, $k$ is the number of non-normal exits of $I$, and $m_1,\dots,m_k$ are integers between 1 and $l+1$ (where it is understood that if $k=0$ the line simply consists of $I$ alone).
[cite:@shepherdsonComputabilityRecursiveFunctions1963;220]
#+end_quote
#+end_figure

***** Annexes
******* Global map's source code

#+header: :noweb strip-export
#+NAME: draw_global_map
# :exports both
#+begin_src python :results value file :var matplot_lib_filename=(org-babel-temp-file "global_map" ".png") :var geonames_key="catgolin"
  <<draw_map>>
  MAP_MARGIN_X = 7
  MAP_MARGIN_Y = 7
  LEGENDS_DEFAULT_OFFSET_X = -15
  LEGENDS_DEFAULT_OFFSET_Y = 30
  LEGENDS_OFFSET_MARGIN_Y = 35

  places = ["Paris", "Solomon Islands", "Marquesas", "Trobriand"]
  return draw_map(places, matplot_lib_filename)
  #+end_src

******* Local map's source code

#+header: :noweb strip-export
#+NAME: draw_local_map
# :exports both
#+begin_src python :results value file :var matplot_lib_filename=(org-babel-temp-file "local_map" ".png") :var geonames_key="catgolin"
    <<draw_map>>
    MAP_MARGIN_X = 0
    MAP_MARGIN_Y = 3
    LEGENDS_DEFAULT_OFFSET_X = 3
    LEGENDS_DEFAULT_OFFSET_Y = 1
    LEGENDS_OFFSET_MARGIN_Y = 3

    islands = ["Fenualoa", "Pileni", "Takataka", "Ua Pou Island", "Highland Papua", "Lifu Island", "Trobriand"]
    return draw_map(islands, matplot_lib_filename)
  #+end_src
