% Created 2024-03-30 sam. 16:12
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{sphere-document}
\usepackage{setspace}
\onehalfspace
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[american]{babel}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\usepackage{wasysym}
\usepackage{setspace}
\usepackage{lastpage}
\usepackage{marginnote}
\usepackage{fmtcount}
\usepackage{float}
\floatstyle{boxed}
\restylefloat{figure}
\newcommand{\fname}[1]{\textsc{#1}}
\makeatletter \let\inserttitle\@title \makeatother
\fancyhead[R]{\vspace*{-1.5cm}Clément \textsc{Cartier}, \textbf{Softwares and String Figures}\\Surprise for Karine \fname{Chemla} - <2024-03-13>}
\doublespacing
\newcommand{\displaytime}[1][0]{
\addtocounter{time}{#1}
\setcounter{minutes}{\value{time}}
\divide\value{minutes} by 60
\setcounter{minutesinseconds}{\value{minutes}}
\multiply\value{minutesinseconds} by 60
\setcounter{seconds}{\value{time}}
\addtocounter{seconds}{-\value{minutesinseconds}}
\marginpar{\textit{(\padzeroes[2]{\decimal{minutes}}'\padzeroes[2]{\decimal{seconds}})}}
}
\author{Clément Cartier\thanks{contact@catgolin.eu}}
\date{\textit{<2024-03-13 mer.>}}
\title{Softwares and String Figures\\\medskip
\large Séminaire \emph{Approche historiques, philosophiques et anthropologiques du nombre, de la mesure et de la mesurabilité}}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={Softwares and String Figures},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={English}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\thispagestyle{fancy}
\setcounter{tocdepth}{2}
\tableofcontents
\newpage

\newcounter{time}
\newcounter{minutes}
\newcounter{seconds}
\newcounter{minutesinseconds}
\setcounter{time}{0}

\begin{abstract}
At least since the fifth edition of Walter William Rouse Ball's \emph{Mathematical Recreations and Essays} published in 1911, the making of string figures has been studied as a kind of mathematical activity.
Analyzing people's performances in making string figures, we can identify this activity as the finite execution of a discrete sequence of operations, where each operation transforms the configuration of the string.
Writing down the finite sequence of instructions corresponding to these operations therefore allow us to define a \emph{program} or a \emph{routine} of string figure-making
Trough the example of different routines used to produce a same ``Owl'' figure, this communication will explore the significance of such a description of string figures with their \emph{programs}.
\end{abstract}

\begin{abstract}
Au moins depuis la cinquième édition de \emph{Mathematical Recreations and Essays} de William Rouse Ball en 1911, la pratique des jeux de ficelles a été étudiée comme une forme d'activité mathématique.
Pour analyser cette pratique, on peut identifier le jeu à l'exécution d'une séquence discrète d'opérations, chacune ayant pour effet de transformer la configuration de la ficelle.
Écrire la séquence d'instructions correspondant à ces opérations nous permet ainsi de définir un \emph{programme} ou une \emph{routine} du jeu de ficelle.
Au travers de l'exemple de différentes routines permettant de produire une même figure de ``chouette'', cette communication discutera de l'apport d'une telle description des jeux de ficelles par leurs programmes.
\end{abstract}

\newpage

\section{Introduction}
\label{sec:orgccdb52c}

For this talk, I wanted to expand on a presentation I made last year with Eric Vandendriessche in the master's course of History and Philosophy of Mathematics.

\displaytime[10]

To begin, I would like to present two different ways of making a figure that was named ``Owl'' by ethnologist Alfred \fname{Tozzer} \footnote{According to his notes, it was called \emph{nas-ja} among the Navaho.} (\citeprocitem{3}{Haddon, 1903} pp.219-220).
The first way, that was shown to Alfred \fname{Haddon} by two old Navaho men he met in Chicago, does not use the classical ``Opening A'' seen in many other string figures to introduce a twist in the index loop on the first movement.
\displaytime[30]

The second way, shown to Caroline Furness \fname{Jayne} by Zah \fname{Tso} and his sister from Gallup in 1904, differs from the first only in the beginning, in that it starts with Opening A before twisting the index loop, and then continues the same as the first procedure (\citeprocitem{4}{Jayne, 1962} pp.54-55).
\displaytime[30]

We should also compare these two string figures with another one, called ``Many Stars'' in English and \emph{Son-tlani} by the Navaho (\citeprocitem{3}{Haddon, 1903}, \citeprocitem{14}{, n.d.} pp.48-53).
It differs from both ``Owls'' by only one step in the beginning: we have the common ``Opening A'', and continues without introducing the twist in the index loop.
Despite follwing almost exactly the same steps, this one has a different name, and a clearly different result.
\displaytime[30]

We could also add a third method for the ``Owl'', introduced by Caroline Furness \fname{Jayne} by a small variation in the making of ``Many Stars'' where, when we introduce the thumbs into the index loop, we proceed from the proximal side instead of the distal side, also leading to a clearly different figure (\citeprocitem{4}{Jayne, 1962} pp.55-56).
\displaytime[30]

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{/tmp/babel-Wbx87y/local_mapjK98bW.png}
\caption{Places where the string figures have been collected (Source: Natural Earth \& geonames.org --- Made with cartopy: source code below)}
\end{figure}

How can we understand the differences and variations between these figures?

String Figures Making have been thought as a kind of ``procedural'' activity\footnote{``The issue of recognizing an activity as mathematical will be addressed in this volume through the analysis of the procedural activity of ''string figure-making``.'' (\citeprocitem{13}{Vandendriessche, 2015} p.4); ``The process of making a string figure can be analysed as a series of ''simple movements`` that I call ''elementary operations``, insofar as the making of any string figure can be described by referring to a certain number of these operations. A string figure can thus be seen as the result of a ''procedure`` consisting of a succession of elementary operations'' (\citeprocitem{13}{Vandendriessche, 2015} p.27)}, described by ethnologists as finite sequences of instructions: This corresponds to a classical definition of a computer program or a routine (see figure \ref{org266c77b}).
Therefore, I would like to think about how applying software engineering methods of version control to the comparison of different procedures for string figures-making raises new questions for the history of mathematical procedures.
\displaytime[30]

\begin{figure}
\begin{quote}
Constructing instruction tables is usually described as ``programming.'' To ``programme a machine to carry out the operation A'' means to put the appropriate instruction table into the machine so that it will do A.
(\citeprocitem{12}{Turing, 1950} p.438)
\end{quote}

\begin{quote}
``So we define a \emph{program} (or routine) as a finite sequence of \(l\) \emph{lines}, each line being of the form \(I[m_1, \dots, m_k]\), where \(I[E1,\dots, Ek]\) is an instruction, \(k\) is the number of non-normal exits of \(I\), and \(m_1,\dots,m_k\) are integers between 1 and \(l+1\) (where it is understood that if \(k=0\) the line simply consists of \(I\) alone).
(\citeprocitem{8}{Shepherdson \& Sturgis, 1963} 220)
\end{quote}
\caption{\label{org266c77b}Quotes on the definition of program}
\end{figure}

\section{Evolving programs}
\label{sec:org7e71328}

When we maintain and work with information systems, we should absolutly avoid thinking of programs, protocols or software applications as static objects.
As soon as a sequence of instructions is copied and executed sufficiently, people will find ways to exploit it's vulnerabilities.
For instance, the security feed from the ANSI's alert center regularly points out critical problems with the Microsoft products massively promoted by our university (Windows, Outlook, Word, etc.)\footnote{See for instance alert CERTFR-2023-ALE-006 from 12 July 2023 on many Microsoft products (\url{https://www.cert.ssi.gouv.fr/alerte/CERTFR-2023-ALE-006/}, consulted on 7 March 2024), or the more recent alert CERTFR-2024-ALE-005 from 15 February 2024 on Microsoft Outlook (\url{https://www.cert.ssi.gouv.fr/alerte/CERTFR-2024-ALE-005/}, consulted on 7 March 2024).}, which are usually resolved by changing the software.
\displaytime[35]

This kind of security problems does not only concern computer programs, but also human procedures. The last report from the public interest group ``Action contre la cybercriminalité'' points out that fishing and false advisors, both executed with the help of carefully written routines, are still the most common attacks on individuals and, in different proportions, for business and public institutions (\citeprocitem{5}{Notin, 2024} pp.31,34-35).
\displaytime[20]

We therefore need understand that any routine that plays a role in a larger system should have the ability to evolve over time in order to keep the system as a whole conform to varying expectations.
\displaytime[15]

Updating part of a routine to stay in complyance with these expectations isn't the same, however, as creating a whole new routine.
We introduce new \emph{versions} of the routine, but we still see these versions as part of the same routine.
In this sense, we could say that ``first'', ``second'' and ``third Owl'' are three versions of the one ``Owl''-making routine.
\displaytime[15]

\subsection{Git versioning}
\label{sec:org3c9b835}

Managing different, and sometimes conflicting versions of a computer program is always a challenge.
Around April 2005, this issue raised lively debates in the open source community maintaining the Linux Kernel, leading some members of this community to develop a now widely adopted tool for this, and to call it Git  (\citeprocitem{11}{Torvalds, 2005c}, \citeprocitem{10}{, 2005b}, \citeprocitem{9}{, 2005a}).
\displaytime[20]

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Git-definition-TORVALDS.png}
\caption{Linus Torvard, ``Initial revision of ''git``, the information manager from hell''. Git, commit e83c516, April 8, 2005 (\url{https://github.com/git/git/commit/e83c5163316f89bfbde7d9ab23ca2e25604af290}, consulted on March 8th 2024)}
\end{figure}

\subsection{Semantic versioning}
\label{sec:org5d2089e}

A few years later, in 2009, Tom Preston-Werner, who was building the popular GitHub website upon Git (\citeprocitem{6}{Preston-Werner, 2008}), started to work on a standard for naming different versions of a software\footnote{The first pre-release version, v0.1.0 dates from December 15 2009 (\url{https://github.com/semver/semver.org/releases/tag/v0.1.0}). The first release candidate was published on November 29 2011 (\url{https://github.com/semver/semver.org/releases/tag/v1.0.0-rc.1}) and the first official release was published on December 23 2011 (\url{https://github.com/semver/semver.org/releases/tag/v1.0.0})}.
\displaytime[20]

Current version of the semantic versioning standard (v2.0.0, available on \url{https://semver.org}) distinguish three kinds of change:
\begin{enumerate}
\item major changes can lead to incompatibilities with previous versions;
\item minor changes add or modify some functionalities, but using it instead of the previous versions should not require any significant adaptation;
\item patches are made to fix bugs or minor issues.
\end{enumerate}
\displaytime[30]

These distinctions are somewhat flexible: applying small patches issued by Microsoft on the university's servers' software would break the compatibility with foreign intelligence services trying to exploit our vulnerabilities.
As stated in the standard, the point is more to make it ``easy to communicate your intentions to the users of your software''.
To present a change as a patch or a minor version instead of a major one does not mean that the routine will yield the same results when used in the same context before or after the change: it means that we expect people not to care too much about the difference.
\displaytime[30]

\begin{figure}
\begin{quote}
By giving a name and clear definition to the above ideas, it becomes easy to communicate your intentions to the users of your software. Once these intentions are clear, flexible (but not too flexible) dependency specifications can finally be made.
(\citeprocitem{7}{Preston-Werner, Haack, Isaacs, Handley, Okushi, Schreijer, Ralph, DannyS712, Tovmach, Donahue, Piasecki, \& Garmon, 2013})
\end{quote}
\end{figure}

If we put the ``first Owl'' and the ``second Owl'' side by side, or if we execute them in succession, I believe most people would not notice a significant difference between both (see figure \ref{org7bfe5a1}).
One could argue that, by introducing ``Opening A'' back into the procedure, the ``second Owl'' is just a patch of the ``first Owl'' which had a weird opening.
If we look close enough, we can still see that they do not produce the same crossings (see figure \ref{orgb9a980d}).
\displaytime[30]

\begin{figure}
\begin{center}
\includegraphics[width=.4\textwidth]{../img/Owl_1-YUAN-2024_03_02.JPG}
\includegraphics[width=.4\textwidth]{../img/Owl_2-YUAN-2024_03_02.JPG}
\end{center}
\caption{\label{org7bfe5a1}First (left) and second (right) owl by Rui \fname{Yuan}, Paris 2nd March 2024}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=.4\textwidth]{../img/Owl_1-YUAN-2024_03_02-crossings.png}
\includegraphics[width=.4\textwidth]{../img/Owl_2-YUAN-2024_03_02-crossings.png}
\end{center}
\caption{\label{orgb9a980d}First (left) and second (right) owl by Rui \fname{Yuan}, Paris 2nd March 2024 with difference in crossings marked in red}
\end{figure}

On the ``third Owl'', however, I argue that the difference is more striking than the other two: when I showed it to Rui, she called it ``a butterfly''.
The fact that we see something new in this figure with respect to the other two justifies that we should consider it like more than a simple patch with respect to the previous versions.
Whether we call it a major or a minor version depends on whether we consider this ``butterfly'' would keep meeting the requirements of people who depended on the previous two versions of the ``Owl''.
\displaytime[30]

I would have much more to say about the ``requirements'' and the ``dependence'' of string figures-making routines, but I only have time for two more notes.
\displaytime[15]

\section{Two more things}
\label{sec:org3d96412}
\subsection{Forks and merges}
\label{sec:orgdaf58aa}

First, the linear succession of semantically versionned releases of a software doesn't mean that they have a linear history:
one important feature of the \emph{git} program is it's ability to allow different people to produce multiple versions in individual sequences called ``branches'', and to merge or synchronise branches.
\displaytime[15]

The strict order between semantically numbered versions does not prevent a more complex history, with a lot more diversity than the simple sequence of versions could show.
Just because we called the them ``first Owl'' and ``second Owl'' doesn't necesseraly mean that one was created before the other: there may have been many branches in the developpement of these two figures, which could have shared part of their history and diverted at some other points.
Alfred \fname{Haddon} simply released the ``first Owl'' in a publication before Zah \fname{Tso} and his sister showed the ``second Owl'' to Caroline Furness \fname{Jayne}.
\displaytime[30]

An open-sourced project usually works by having people ``fork'' the program to customize it on their own machine, adapt it to their needs and to the constraints of a new support, before trying to merge their branch with that of other developers.
In her anthropological study of French school's playgrounds published in 2001, Julie Delalande observed a similar process for the evolution of games played by children:
\begin{quote}
By playing together, each brings his or her own personal note and tries to make it accepted by others. As in the rope game, transformations come less from an imitative process than from an adaptation to the players' abilities and from their desire to add their small innovations to the game. […] Variants allow a group of friends to make their own a game practiced by all. The addition of an element, of a difficulty, gives a familiar game a new attraction.\footnote{\emph{En jouant à plusieurs, chacun apporte sa note personnelle et cherche à la faire accepter des autres. Comme au jeu de corde, les transformations viennent moins d'un processus imitatif que d'une adaptation aux capacités des joueurs et de leur désir d'ajouter au jeu leurs petites innovations. […] Les variantes permettent à un groupe d'amis de s'approprier un jeu pratiqué par tous. L'ajout d'un élément, d'une difficulté, donne un attrait nouveau à un jeu familier.}
(\citeprocitem{1}{Delalande, 2001})}
\end{quote}
Just as computer programs have to be adapted to hardware's specifications and to users' needs, games ---including string figures-making--- evolve to match the players' abilities and wants.
\displaytime[45]

I don't have time to talk about this, but Julie \fname{Delalande}'s work about children's games also gives interresting insight into how these individual branches are merged and synchronized together in the playground.
\displaytime[15]

\subsection{Names}
\label{sec:org9d9a261}

Second, one version can have a name, independently of it's ``semantic version''.
Android versions, for instance, are associated with desert or sweet names, independantly of the version number.
If semantic versions allows to keep track of compatibility issues, other version names can be used in parallel to signal other aspects.
\displaytime[30]

We could therefore choose to name the ``third Owl'' as the ``butterfly'': it would still be a version of the ``Owl'', but it would communicate the fact that we also start to see something else in this version, that wasn't in the previous ones.
\displaytime[30]

Adding a name to an existing routine could, in itself, be a way to produce a new version of the same routine.
Indeed, Apple II computer's reference manual from 1979 defined a ``feature'' as ``A \emph{bug} as described by the marketing department'' (\citeprocitem{2}{Espinosa, 1979} p.180), and a simple re-naming of some parts of the code can sometime justify a full new version.
This importance of names has also been observed by Julia Delalande in children's plays:
\begin{quote}
Each child is free to give the game a new appeal by giving a new name for the sand he gets from a particular place. This is why, if part of the technique is relatively stable […], the room for invention is large and can allow a child to get noticed by his peers.\footnote{\emph{Chaque enfant est libre de donner un attrait supplémentaire au jeu en trouvant un nouveau nom pour un sable qu'il ira chercher à un endroit particulier. C'est pourquoi, si une partie des techniques est relativement stable (notamment la manière de gratter le sol de ses talons), la place de l'invention est grande et elle peut permettre à un enfant de se faire remarquer de ses pairs. [\ldots{}] Le statut de leader se confirme d'ailleurs en faisant preuve d'invention dans les jeux de sable.}
(\citeprocitem{1}{Delalande, 2001} §18)}
\end{quote}
We could be tempted to overlook names, stories, songs, sounds, and many other elements evolving around the rountine when deciding of the importance of a change, but they are actually important to define what we are able to do with it.
Updating a computer program's documentation, or changing the \emph{vinavina} associated to a \emph{kianinikula} in the Oluvilei village could sometimes lead to a major change in the semantic version, even though the routine was almost not transformed.
\displaytime[45]

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Bug_feature-FORESTER_JOE-2008.jpg}
\caption{``It's not a BUG Its a FEATURE'' (Forester-Joe, deviantart.com, 30 June 2008, made with Paint.Net v3.31: \url{https://www.deviantart.com/forester-joe/art/Its-not-a-BUG-Its-a-FEATURE-90226561} ©CC-BY-NC)}
\end{figure}

\section{Conclusion}
\label{sec:org87bb075}

During this presentation, I didn't speak too much about the history of mathematics.
To conclude, we could ask why someone interested in medieval instruments dedicated to astronomical computations should worry about contemporary children's plays and string figures.
\displaytime[15]

This summer, I talked with Glenn \fname{Van Brummelen} who told me that, what he found very refreshing in \textsc{Sphere}, and especially in Karine \fname{Chemla}'s work, was a tendency to take well-established ideas, and hit them with a conceptual hammer to see what happens when they break: what comes out of the pieces is usually far more interresting than the original, and brings new ideas for many different fields.
\displaytime[15]

I feel like string figures are good candidates for a Chemlaian's hammer because, if we try to take them seriously, they lead us to question important concepts and how we use them to understand both ancient and contemporary practices.
Here, I tried to show that, if we consider string figures-making to be a kind of ``procedural'' activity, the comparison with the way computer programs are written with applications such as ``git'' allows us to understand how the routine can evolve while it is passed through the hands of different persons.
This is will also be relevant for instruments designed for astronomical computations, especially since, for them, we have written texts that are copied to describe the procedure, making it easier to track the changes made between one version and the other.
\displaytime[30]

There are, in my opinion, three main lessons we can take from this exercise:
\begin{enumerate}
\item Any routine is bound to evolve if it is to stay relevant, and the multiplicity of versions does not necessarily contradict the stability of the procedure.
\displaytime[15]
\item This evolution is almost never linear: it depends on many different people making their own versions, merging them together, and agreeing on which parts to keep and which to remove, even if this means producing diverging versions.
\displaytime[15]
\item There is no universal rule to decide which transformation is important and which is minor: in the end, it depends on peoples' expectations, which is not only based on the raw results a routine can yield, but also by how it is described, layed out and understood. This depends on the routine, but also on it's execution supports\footnote{For instance, Enrique which Éric \fname{Vandendriessche} met in the Santa Teresita mission, used different fingers to perform the same routine than what he had learned, because his right index was shorter than his left: ``At first I thought that Enrique was concentrating more on the movements of the strings than on movements of fingers. Yet, when viewing his performance on video afterwards, I noticed a detail that I had overlooked during the working session: Enrique has a shorter index finger on his right hand, which was probably cut off accidentally. It was clear that he had modified his way of making string figures because of his handicap.'' (\citeprocitem{13}{Vandendriessche, 2015} p.264).}, the extant documentation, among other things, which also justifies the need for multiple alternatives for a same routine.
\displaytime[30]
\end{enumerate}

I think this hammer can also help us to come to peace with our changing world: things evolve and, in most cases, a new version of the thing doesn't erase previous ones. Just as the ``Owl'' doesn't disappear if I perform openning A before the twist, or if I practice the ``butterfly'' variation, the French language won't disappear because someone decided to experiment putting dots between ``Le'' and ``La'' (see figure \ref{fig:org8648d66}).
On the contrary, diversity allows for a most needed adaptation to our world: we need more of it in both our computers and our societies.
I think reading and working with Karine \fname{Chemla}'s approach in the history and philosophy of mathematics is a good way to better understand how.
\displaytime[15]

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/THEUTH-ecriture_inclusive-CUNIN-2024.png}
\caption{\label{fig:org8648d66}Unintelligible message from Patrice Cunin, ``À propos du féminisme et de l'écriture inclusive - Re: Poste ATER en STS au Cnam'', Theuth, 8 March 2024}
\end{figure}
\section{Bibliography}
\label{sec:org9a8bf20}
\hypertarget{citeproc_bib_item_1}{[1] Julie Delalande, « Chapitre VI. Les jeux comme faits de culture » \textit{La cour de récréation : Pour une anthropologie de l’enfance}. 2001 (fr-FR: en ligne: \url{https://books.openedition.org/pur/24152} - consulté le 11 mars 2024).}

\hypertarget{citeproc_bib_item_2}{[2] Christopher Espinosa, \textit{Apple II} 1979 (en-US: Reference manual, en ligne: \url{http://www.apple-iigs.info/doc/fichiers/appleiiref.pdf} - consulté le 11 mars 2024).}

\hypertarget{citeproc_bib_item_3}{[3] Alfred Cort Haddon, « A Few American String Figures and Tricks » \textit{American Anthropologist}. 1903 (en-US: en ligne: \url{https://anthrosource.onlinelibrary.wiley.com/doi/10.1525/aa.1903.5.2.02a00020} - consulté le 29 février 2024).}

\hypertarget{citeproc_bib_item_4}{[4] Caroline Furness Jayne, \textit{String Figures and How To Make Them. A Study of Cat’s-Cradle in Many Lands} 1962 (en-US: en ligne: \url{http://archive.org/details/stringfigureshow00jayn} - consulté le 29 février 2024).}

\hypertarget{citeproc_bib_item_5}{[5] Jérôme Notin, \textit{Au cœur de l’action Cyber} 2024 (fr-FR: Rapport d’activité 2023, en ligne: \url{https://www.cybermalveillance.gouv.fr/medias/2024/02/240226_RA_2023_SCREEN.pdf} - consulté le 7 mars 2024).}

\hypertarget{citeproc_bib_item_6}{[6] Tom Preston-Werner, « GitHub Turns One! » 2008 (en-US: en ligne: \url{https://github.blog/2008-10-19-github-turns-one/} - consulté le 7 mars 2024).}

\hypertarget{citeproc_bib_item_7}{[7] Tom Preston-Werner, Phil Haack, Isaacs, et al., \textit{Semantic Versioning Specification} 2013 (en ligne: \url{https://github.com/semver/semver}).}

\hypertarget{citeproc_bib_item_8}{[8] J. C. Shepherdson \& H. E. Sturgis, « Computability of Recursive Functions » \textit{Journal of the Acm}. 1963 (en-US: en ligne: \url{https://dl.acm.org/doi/10.1145/321160.321170} - consulté le 9 février 2024).}

\hypertarget{citeproc_bib_item_9}{[9] Linus Torvalds, « First Ever Real Kernel Git Merge! » 2005a (en-US: en ligne: \url{https://marc.info/?l=git&m=111377572329534}).}

\hypertarget{citeproc_bib_item_10}{[10] Linus Torvalds, « Re: Kernel SCM Saga. » 2005b (en-US: en ligne: \url{https://marc.info/?l=linux-kernel&m=111288700902396}).}

\hypertarget{citeproc_bib_item_11}{[11] Linus Torvalds, « Re: Kernel SCM Saga. » 2005c (en-US: en ligne: \url{https://marc.info/?l=linux-kernel&m=111280216717070}).}

\hypertarget{citeproc_bib_item_12}{[12] Alan Mathison Turing, « Computing Machinery and Intelligence » \textit{Mind. A Quarterly Review of Psychology and Philosophy}. 1950 (en-US: en ligne: \url{https://doi.org/10.1093/mind/LIX.236.433} - consulté le 26 février 2024).}

\hypertarget{citeproc_bib_item_13}{[13] Eric Vandendriessche, \textit{String Figures as Mathematics? An Anthropological Approach to String Figure-making in Oral Tradition Societies} 2015 (en-US: en ligne: \url{https://link.springer.com/10.1007/978-3-319-11994-6} - consulté le 8 février 2024).}

\hypertarget{citeproc_bib_item_14}{[14]. }
\section{Annexes}
\label{sec:org4a463ed}
\subsection{Source code for drawing maps}
\label{sec:org04dc686}
\begin{minted}[]{python}
""" Plotting locations on a map """

import geocoder
import matplotlib.pyplot as plt
import cartopy
from cartopy.mpl.geoaxes import GeoAxes

Place = tuple[str, float, float] # Name, longitude, latitude
Legend = tuple[
    str,                         # Text of the legend
    float,                       # Text's x coordinate
    float,                       # Text's y coordinate
    float,                       # Point's x coordinate
    float,                       # Point's y coordinate
]
Position = tuple[float, float]  # x, y

MAP_MARGIN_X = 1              # Distance from western point to map's border
MAP_MARGIN_Y = 1              # Distance from southern point to map's border
LEGENDS_DEFAULT_OFFSET_X = .5 # Distance from a point to it's legend (x)
LEGENDS_DEFAULT_OFFSET_Y = .5 # Distance from a point to it's legend (y)
LEGENDS_OFFSET_MARGIN_Y = .1  # Distance between two legends

def draw_map(places_names: list[str], filename: str, meridian: float = 159) -> str:
    ax = init_map(meridian)
    places = [get_place_informations(name, meridian) for name in places_names]
    draw_places(places, ax)
    plt.tight_layout()
    plt.savefig(filename)
    return filename

def init_map(meridian: float = 159) -> GeoAxes:
    ax = plt.axes(projection=cartopy.crs.PlateCarree(central_longitude=meridian))
    ax.coastlines()
    ax.add_feature(cartopy.feature.OCEAN)
    ax.add_feature(cartopy.feature.LAKES)
    ax.add_feature(cartopy.feature.LAND, alpha=.3)
    ax.add_feature(cartopy.feature.RIVERS)
    ax.add_feature(cartopy.feature.STATES, alpha=.1)
    return ax

def get_place_informations(name: str, meridian: float = 180) -> tuple[str, float, float]:
    geonames_id = geocoder.geonames(name, key=geonames_key).geonames_id
    data = geocoder.geonames(geonames_id, method="details", key=geonames_key)
    longitude = float(data.lng)
    return (data.address, longitude, float(data.lat))

def draw_places(places: list[Place], ax: GeoAxes):
    legends = []
    for place in places:
        draw_place(place, ax)
        compute_legend_position(place, legends)
    for legend in legends:
        draw_legend(legend, ax)

def draw_place(place: Place, ax: GeoAxes):
    ax.plot([place[1]], [place[2]], marker="o", color="red", markersize=4, transform=cartopy.crs.PlateCarree())
    # Make sure to have margins
    ax.plot([place[1] + MAP_MARGIN_X], [place[2] + MAP_MARGIN_Y], transform=cartopy.crs.PlateCarree())
    ax.plot([place[1] - MAP_MARGIN_X], [place[2] - MAP_MARGIN_Y], transform=cartopy.crs.PlateCarree())

def draw_legend(legend: Legend, ax: GeoAxes):
    ax.annotate(
        legend[0],
        xytext=(legend[1], legend[2]),
        xy=(legend[3], legend[4]),
        arrowprops=dict(arrowstyle="->"),
        fontsize=12, wrap=True,
        horizontalalignment="center",
        transform=cartopy.crs.PlateCarree(),
    )

def compute_legend_position(place: Place, other_legends: list[Legend]):
    x = place[1] + LEGENDS_DEFAULT_OFFSET_X
    y = compute_legend_y((x, place[2]), other_legends)
    other_legends.append((place[0], x, y, place[1], place[2]))

def compute_legend_y(pos: Position, other_legends: list[Legend]) -> float:
    offset = LEGENDS_DEFAULT_OFFSET_Y
    for other in other_legends:
        offset = avoid_conflict(pos, offset, other)
    return pos[1] + offset

def avoid_conflict(pos: Position, offset: float, other: Legend) -> float:
    new_pos = (pos[0], pos[1] + offset)
    if are_legends_conflicting(new_pos, other):
        return offset * -1
    return offset

def are_legends_conflicting(pos: Position, other: Legend) -> bool:
    return LEGENDS_OFFSET_MARGIN_Y > abs(pos[1] - other[2])

def is_above(pos: tuple[float, float], other: Legend) -> bool:
    return pos[1] >= other[2]

\end{minted}

\begin{minted}[]{python}

MAP_MARGIN_X = 7
MAP_MARGIN_Y = 10
LEGENDS_DEFAULT_OFFSET_X = 0
LEGENDS_DEFAULT_OFFSET_Y = 3
LEGENDS_OFFSET_MARGIN_Y = 3

places = ["New Mexico", "Chicago", "Nanizhoozhi"]
return draw_map(places, matplot_lib_filename)
\end{minted}
\end{document}