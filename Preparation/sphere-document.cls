\ProvidesClass{sphere-document}

\LoadClass[a4paper, 12pt]{article}

\RequirePackage{xcolor}

\definecolor{sphere}{HTML}{D8213B}

\RequirePackage[a4paper, left=2.5cm, right=2.5cm, top=3.5cm, bottom=2.5cm]{geometry}

\RequirePackage{fancyhdr}
\renewcommand{\headrulewidth}{0}

\pagestyle{fancy}

\fancyhead[L]{
  \vspace*{-1.31cm}
  \hspace{-1.54cm}
  \includegraphics[height=1.6cm]{sphere-templates/SPHERE-logo-couleurs-horizontal-2023.png}
}

\fancyfoot[L]{
  \vspace{-0.31cm}
  \tiny
  \hspace{-2cm}
  Université Paris Cité / Laboratoire \textsc{Sphere} UMR 7219\\
  \hspace{-2cm}
  case courrier 7093 - 27 rue Jean Antoine le Baïf 75013 Paris\\
  \hspace{-2cm}
  Tel +33 1 57 27 63 24 -- Fax +33 1 57 27 63 29\\
  \hspace{-2cm}
  http://www.sphere.univ-paris-diderot.fr\\
}
\fancyfoot[C]{
  \vspace{1.1cm}
  {\color{sphere}\hspace*{-2.5cm}\rule{21cm}{.36cm}}
}
\fancyfoot[R]{
  \vspace{-.5cm}
  \thepage/\pageref{LastPage}
  \hspace{.5cm}
  \includegraphics[height=1.2cm]{sphere-templates/CNRS-logo-couleurs-carre-2023.png}
  \hspace{1.2cm}
  \includegraphics[height=1.2cm]{sphere-templates/UPCite-logo-couleurs-carre-2022.png}
  \hspace{1.2cm}
  \includegraphics[height=1.2cm]{sphere-templates/PantheonSorbonne-logo-couleurs-horizontal-2015.png}
  \hspace*{-2cm}
}
