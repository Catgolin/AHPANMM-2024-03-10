\ProvidesClass{sphere-letter}

\LoadClass[a4paper, 12pt]{letter}

\RequirePackage{svg}
\RequirePackage{xcolor}
\RequirePackage{lmodern}

\definecolor{sphere}{HTML}{D8213B}

\RequirePackage{setspace}
\onehalfspace

\RequirePackage[a4paper, left=2.5cm, right=2.5cm, top=2.5cm, bottom=2.5cm]{geometry}

\RequirePackage{fancyhdr}
\renewcommand{\headrulewidth}{0}

\fancyhead[L]{
  \vspace{0.35cm}
  \hspace{-1.54cm}
  \includegraphics[height=1.2cm]{./SPHERE-logo-couleurs-horizontal-2023.png}
}
\fancyhead[R]{
  \includegraphics[height=1.2cm]{./CNRS-logo-couleurs-carre-2023.png}
  \hspace{2.4cm}
  \includegraphics[height=1.2cm]{./UPCite-logo-couleurs-carre-2022.png}
  \hspace{1.2cm}
  \includegraphics[height=1.2cm]{./PantheonSorbonne-logo-couleurs-horizontal-2015.png}
}
\fancyfoot[C]{
  \tiny
  Université Paris Cité / Laboratoire \textsc{Sphere} UMR 7219 - case courrier 7093 - 27 rue Jean Antoine le Baïf 75013 Paris\\
  Tel +33 1 57 27 63 24 -- Fax +33 1 57 27 63 29\\
  \url{http://www.sphere.univ-paris-diderot.fr}\\
  \vspace{.7cm}
  {\color{sphere}\hspace*{-2.5cm}\rule{21cm}{.36cm}}
}
